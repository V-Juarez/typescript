console.log("String")
type Person = {
  name: string;
};

const person: Person = {
  name: "John",
};

console.log(person);

let myName: string = "HuXn WebDeve"
console.log(myName)

console.log("Number")

let myNumber: number = 10
console.log(myNumber)

console.log("Boolean")
let myBoolean: boolean = true
console.log(myBoolean)

let tsHard: boolean = false
tsHard = true
console.log(tsHard)
