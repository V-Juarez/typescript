// Nico => [N, i, c, o] => string => string[]
// [N, i, c, o] => Nico => String[] => string

function parseStr(input: string | string[]): string | string[] {
  if (Array.isArray(input)) {
    return input.join(''); // String
  } else {
    return  input.split(''); // string[]
  }
}

const rtaArray = parseStr('Nico');
if (Array.isArray(rtaArray)) {
  rtaArray.reverse();
}
console.log('rtaArray: ',  'Nico =>', rtaArray);

const rtaStr = parseStr(['N', 'i', 'c', 'o']);
if (typeof rtaStr === 'string') {
  rtaStr.toLocaleLowerCase();
}
console.log('rtaArray: ', "['N', 'i', 'c', 'o'] =>", rtaStr)
