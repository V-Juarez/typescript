const prices: (number | string)[] = [1,2,3,4,5,6, "ass"];
prices.push(1);
prices.push("Hola");

console.log(prices);

// Determinar al incio las variables.
let user: [string, number];
user = ['nicobytes', 12];
user = ['12', 23]

// Error in the tuple
// user = [];
// user = ['nico']
// user = ['nico', 12]
// user = ['nico', 12, true]
console.log(user[0], user[1]);

const [username, age] = user;
console.log(username);
console.log(age);




