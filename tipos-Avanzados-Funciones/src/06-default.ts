export const createProduct = (
  id: string | number,
  isNew: boolean = true,
  stock: number = 10,
) => {
  return {
    id,
    stock,
    isNew,
  }
}

/**
 * El operador || toma los parametors establecidos si esta vacio o es 0. El campo no existe
 * todoToma los valores por defecto, al declarar false o 0.
 * ? 0 === false
 * @'' === false
 * *false === false
 */

const p1 = createProduct(1, true, 12);
const p2 = createProduct(2, false, 0);
const p3 = createProduct(3);
const p4 = createProduct(4, true, 100);
const p5 = createProduct(4, true);

console.log(p1)
console.log(p2)
console.log(p3)
console.log(p4)
console.log(p5)
