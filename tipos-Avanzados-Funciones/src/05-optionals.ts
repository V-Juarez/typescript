export const createProduct = (
  id: string | number,
  isNew?: boolean,
  stock?: number,
) => {
  return {
    id,
    stock: stock ?? 10,
    isNew: isNew ?? true,
  }
}

/**
 * El operador || toma los parametors establecidos si esta vacio o es 0. El campo no existe
 * todoToma los valores por defecto, al declarar false o 0.
 * ? 0 === false
 * @'' === false
 * *false === false
 */

const p1 = createProduct(1, true, 12);
const p2 = createProduct(2, false, 0);
const p3 = createProduct(3);

console.log(p1)
console.log(p2)
console.log(p3)
