export abstract class Animal {
  constructor(protected name: string) {}

  move() {
    console.log('Moving along!');
  }

  greeting() {
    return `Hello, I'm ${this.name}`;
  }

  protected doSommething() {
    console.log('doo');
  }
}

export class Dog extends Animal {
  constructor(name: string, public owner: string) {
    super(name);
  }

  woof(times: number): void {
    for (let index = 0; index < times; index++) {
      console.log(`Woof! ${this.name}`);
    }
    this.doSommething();
  }

  move() {
    console.log('Moving as a dog');
    super.move();
  }
}

const cheis = new Dog('Cheis', 'Paw');
// cheis.name = 'Dorama';
cheis.woof(3);
cheis.move();
