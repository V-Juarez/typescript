console.log('Math.PI', Math.PI);
console.log('Math.max', Math.max(1, 2, 2, 6, 3, 2, 1));

// estatico y no editable
class MyMath {
  static readonly PI = 3.14;

  static max(...numbers: number[]) {
    // console.log(numbers);
    // return numbers[0];
    return numbers.reduce((max, item) => (max >= item ? max : item), 0);
  }
}

// const math = new MyMath();
// math.PI;
console.log('MyMath.PI', MyMath.PI);
console.log('MyMath.max', Math.max(112, 2, 1, 12, 9));
const numbers = [12, 3, 4, 3, 2, 123, 234];
console.log('MyMath.max', MyMath.max(...numbers));

console.log("Hello world");
console.log("Todo esta listo");
