export class MyDate {
  constructor(
    public year: number = 1993,
    private _month: number = 7,
    private _day: number = 9
  ) {}

  printFormat(): string {
    const _day = this.addPadding(this._day);
    const month = this.addPadding(this.month);
    return `${_day}/${month}/${this.year}`;
  }

  private addPadding(value: number) {
    if (value < 10) {
      return `0${value}`;
    }
    return `${value}`;
  }

  public add(amount: number, type: '_days' | 'months' | 'years') {
    if (type === '_days') {
      this._day += amount;
    }
    if (type === 'months') {
      this.month += amount;
    }
    if (type === 'years') {
      this.year += amount;
    }
  }

  get day() {
    return this._day;
  }

  get isLeapyear(): boolean {
    if (this.year % 400 === 0) return true;
    if (this.year % 100 === 0) return false;
    return this.year % 4 === 0;
  }

  get month() {
    return this._month;
  }

  // set month(newValue: number) {
  //   if (newValue >= 1 && newValue <= 12) {
  //     this._month = newValue;
  //   } else {
  //     throw new Error('month out of range');
  //   }
  // }
  set month(value: number) {
    try {
      if (value >= 1 && value <= 12) {
        this._month = value;
      } else {
        throw new Error('month out of range');
      }
    } catch (e) {
      const error = (e as Error).message;
      console.log(error);
    }
  }
}

const myDate = new MyDate(2021, 3, 12);
console.log(myDate.printFormat());
myDate.month = 4;
console.log('run', myDate.month);

myDate.month = 78;
console.log('Esto no debe aparecer', myDate.month);
