import { Animal, Dog } from './09-protected';

const animal = new Animal('elite');
animal.greeting();

const cheis = new Dog('Cheis');
cheis.greeting();
cheis.woof(2);

abstract class Pet {} // no instancias de esta clase

class Dog extends Pet {} // de esta si podemos
