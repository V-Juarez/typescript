import { MyDate } from './01-class';

const myDate = new MyDate(1993, 7, 9);
console.log(myDate.printFormat());

myDate.add(2, 'days');
console.log(myDate.printFormat());

myDate.add(1, 'months');
console.log(myDate.printFormat());

console.log(myDate.day);
console.log(myDate.month);
console.log(myDate.year);
