export class MyDate {
  constructor(
    public year: number = 1993,
    public month: number = 7,
    private _day: number = 9
  ) {}

  printFormat(): string {
    const _day = this.addPadding(this._day);
    const month = this.addPadding(this.month);
    return `${_day}/${month}/${this.year}`;
  }

  private addPadding(value: number) {
    if (value < 10) {
      return `0${value}`;
    }
    return `${value}`;
  }

  public add(amount: number, type: '_days' | 'months' | 'years') {
    if (type === '_days') {
      this._day += amount;
    }
    if (type === 'months') {
      this.month += amount;
    }
    if (type === 'years') {
      this.year += amount;
    }
  }

  get day() {
    return this._day;
  }

  get isLeapyear(): boolean {
    if (this.year % 400 === 0) return true;
    if (this.year % 100 === 0) return false;
    return this.year % 4 === 0;
  }
}

const myDate = new MyDate(2021, 3, 12);
console.log(myDate.printFormat());
// console.log(myDate.getday());

console.log(myDate.day);
console.log('1993', myDate.isLeapyear);

const myDate2 = new MyDate(2000, 7, 10);
console.log('2000', myDate2.isLeapyear);

const myDate3 = new MyDate(2001, 7, 10);
console.log('2001', myDate3.isLeapyear);

const myDate4 = new MyDate:(2002, 7, 10);
console.log('2002', myDate4.isLeapyear);

const myDate5 = new MyDate(2004, 7, 10);
console.log('2004', myDate5.isLeapyear);

console.log("Hola")
