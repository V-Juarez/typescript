## Create gitignore file

- [x] [gitignore.io](https://www.toptal.com/developers/gitignore/)

## install typescript

```sh
npm install typescript --save-dev
```

**Version**

```sh
npx tsc --version
```


## Compilar TS

```sh
npx tsc src/demo.ts --target es6
```

## Save directory `dist`
```sh
 ❯ npx tsc src/*.ts --target es6 --outDir dist

# --target es6 -> version Javascript
# --outDir dist -> Save file in the directory dist
```

## Inicializa un archivo tsconfig.ts. 

```sh
npx tsc --init 
```


Configuración como el target, ourDir, strictMode, etc. Evitándonos tener que poner esas flags en cada compilación.
.
Una vez con ese archivo, solo corremos el comando npx tsc y listo.

compilación continua corriendo el comando 

```sh
npx tsc --watch
```
