(()=> {
  let prices = [1,2,3,4,5];
  // prices.push('asss');
  // prices.push(true);
  // prices.push({});
  prices.push(121212);

  // let products = ['hola', true];
  // products.push(12);

  const mixed: (number | string | boolean | Object)[] = ['hola', true]
  mixed.push(12);
  mixed.push('as');
  mixed.push(true);
  mixed.push({});
  mixed.push([]);
  mixed.push(14);

  let numbers = [1,2,3,4,5,6]
  numbers.map(item => item * 2);

  console.log(numbers);
})();
