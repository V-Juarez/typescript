import { addProduct, calcStock, products } from "./product.service";

addProduct({
  name: 'Prod1',
  createdAt: new Date(1993, 1, 1),
  stock: 7
});
addProduct({
  name: 'Pro2',
  createdAt: new Date(2022, 1, 1),
  stock: 4,
  size: 'XL'
});
console.log(products);
const total = calcStock();
console.log(total);
console.log(products);
