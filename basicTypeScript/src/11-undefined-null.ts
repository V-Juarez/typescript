(() => {
/*   let miNumber: number = undefined;
  let miString: string = null; */

  let myNull: null = null;
  let myUndefined: undefined = undefined;

  let niNumber: number | null = null;
  niNumber = 12;

  let miString: string | undefined = undefined;
  miString = 'as';

  function hi(name: string | null) {
    let hello = "Hola ";
    hello += name?.toLocaleLowerCase() || 'nobody';
    console.log(hello);
  }
/*   function hi(name: string | null) {
    let hello = 'Hola ';

    if (name) {
      hello += name;
    } else {
      hello += 'nobody';
    }
    console.log(hello);
  } */

  hi('Gomez');
  hi('null');
})();

