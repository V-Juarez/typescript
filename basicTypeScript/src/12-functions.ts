(() => {

  type Sizes = 'S' | 'M' | 'L' | 'XL';

  function createProductToJson(
    title: string,
    createdAt: Date,
    stock: number,
    size: Sizes,
  ) {
    return {
      title,
      createdAt,
      stock,
      size
    }
  }
  const prod1 = createProductToJson('P1', new Date(), 12, 'XL');
  console.log(prod1);
  console.log(prod1.title);
  console.log(prod1.createdAt);
  console.log(prod1.stock);



  const createProductToJsonV2 = (
    title: string,
    createdAt: Date,
    stock: number,
    size?: Sizes,
  ) => {
    return {
      title,
      createdAt,
      stock,
      size
    }
  }
  const prod2 = createProductToJson('P1', new Date(), 12);
  console.log(prod1);
  console.log(prod1.title);
  console.log(prod1.createdAt);
  console.log(prod1.stock);
})();
